# Import needed Flask components.
from flask import session, redirect, url_for, render_template

# Import the FlaskView template.
from flask.ext.classy import FlaskView

# Module code
class SearchView(FlaskView):
	def index(self):
		return render_template('search.html')
