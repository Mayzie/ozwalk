# Import needed Flask components.
from flask import session, redirect, url_for, render_template

# Import the FlaskView template.
from flask.ext.classy import FlaskView

# Import required templates for redirect.
from .root	import RootView

# Module code
class LogoutView(FlaskView):
	def index(self):
		session.pop('logged_in', None)
		return redirect(url_for('RootView:index'))
