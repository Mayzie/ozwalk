# Import needed Flask components.
from flask import session, redirect, url_for, render_template

# Import the FlaskView template.
from flask.ext.classy import FlaskView

# Module code
class RootView(FlaskView):
	route_base = '/'

	def index(self):
		return render_template('index.html')
