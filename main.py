from werkzeug.debug import DebuggedApplication

# Import Flask.
from flask import Flask

# Import all of the available views.
from views.root		import RootView		# URL: /
from views.login	import LoginView	# URL: /login/
from views.logout	import LogoutView	# URL: /logout/
from views.search	import SearchView	# URL: /search/
#from views.track	import TrackView	# URL: /track/
#from views.profile	import ProfileView	# URL: /profile/

# Initialise the Flask application.
app = Flask(__name__)
app.config['PROPOGATE_EXCEPTIONS'] = True
app.secret_key = 'RaNdOm-EnCrYpTiOn-KeY'
app.wsgi_app = DebuggedApplication(app.wsgi_app, True)
app.debug = True

# Initialise all of the Flask views.
RootView.register(app)
LoginView.register(app)
LogoutView.register(app)
SearchView.register(app)
#TrackView.register(app)
#ProfileView.register(app)

if __name__ == '__main__':
	app.run(debug=True)
