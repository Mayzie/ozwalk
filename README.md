# OzWalk

This is just a private project to help me develop my web application skills.

## About

OzWalk is designed to be a web application that promotes bushwalking in 
Australia/New Zealand and (hopefully) around the globe. It provides a database 
of bushwalk sites throughout various defined regions and allows users to 
contribute to this database. It allows users to search for walks via keywords 
with several "advanced search" options available.

Each walk is called a `track`, and within a track are several `waypoints` and 
`edges` (will probably rename the latter term, as it is rather technical). A 
track is simply a graph represented in the database.

* Waypoints are guides ensuring walkers remain on the correct route, and are 
accompanied by images of the site as-well as a general description of what to 
expect. If the waypoint is a `special` waypoint (rest-stop, camping area, 
or commercial site), then it can be accompanied by additional information (e.g. 
 are toilets available, are campfires permitted, is water safe for drinking, etc) 
which will be displayed to the end user.

* Waypoint edges detail how to get from one waypoint to the next, including 
estimated duration, ascent/descent, and distance.

Waypoints and waypoint edges are not unique to a track. For example, if two 
tracks overlap, then the affected waypoints and edges will remain uniform across 
both tracks.

### tl;dr

It is a wiki for walks.

## Requirements

### Backend

Please note, this section is subject to change as the project get closer to 
production (it may require additional software, e.g, Redis or the like).

* Python 3+ (developed on Python 3.4)
* PostgreSQL 9+ (developed on PostgreSQL 9.3)

### Frontend

I plan to target HTML5 browsers, such as:

* Internet Explorer 9+
* Mozilla Firefox
* Google Chrome


