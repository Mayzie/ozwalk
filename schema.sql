﻿BEGIN TRANSACTION;
DROP SCHEMA IF EXISTS OzWalk CASCADE;

CREATE SCHEMA OzWalk;

SET search_path = OzWalk, '$user', public;
SET CONSTRAINTS ALL DEFERRED;

CREATE TABLE OzWalk.Countries (
	country_id	SERIAL,
	country_name	VARCHAR(64) UNIQUE NOT NULL,
	--
	PRIMARY KEY (country_id)
);

CREATE TABLE OzWalk.States (
	state_id	SERIAL,
	state_name	VARCHAR(64) NOT NULL,
	state_abbr	VARCHAR(8) NOT NULL,
	country_id	INTEGER NOT NULL,
	--
	CONSTRAINT uniqueStateCountry UNIQUE(state_name, country_id),
	FOREIGN KEY (country_id) REFERENCES OzWalk.Countries(country_id) ON DELETE CASCADE,
	PRIMARY KEY (state_id)
);

CREATE TABLE OzWalk.Regions (
	region_id	SERIAL,
	region_name	VARCHAR(128) NOT NULL,
	state_id	INTEGER NOT NULL,
	--
	CONSTRAINT uniqueRegionState UNIQUE(region_name, state_id),
	FOREIGN KEY (state_id) REFERENCES OzWalk.States(state_id) ON DELETE CASCADE,
	PRIMARY KEY (region_id)
);

CREATE TABLE OzWalk.WaypointTypes (
	waypointtype_id	SERIAL,
	waypointtype_name	VARCHAR(64) UNIQUE NOT NULL,
	waypointtype_style	VARCHAR(128),
	waypointtype_parent_id	INTEGER,
	--
	FOREIGN KEY (waypointtype_parent_id) REFERENCES OzWalk.WaypointTypes(waypointtype_id) ON DELETE CASCADE,
	PRIMARY KEY (waypointtype_id)
);

CREATE TABLE OzWalk.WaypointPropertyCategories (
	waypointpropertycat_id	SERIAL,
	waypointpropertycat_name	VARCHAR(64) UNIQUE NOT NULL,
	waypointpropertycat_text_prepend	TEXT,
	waypointpropertycat_text_append	TEXT,
	waypointpropertycat_waypointtype_id	INTEGER NOT NULL,
	--
	FOREIGN KEY (waypointpropertycat_waypointtype_id) REFERENCES OzWalk.WaypointTypes(waypointtype_id) ON DELETE CASCADE,
	PRIMARY KEY (waypointpropertycat_id)
);

CREATE TABLE OzWalk.WaypointPropertyDefinitions (
	waypointpropertydef_id	SERIAL,
	waypointpropertydef_name	VARCHAR(64) UNIQUE NOT NULL,
	waypointpropertydef_on_true	TEXT,
	waypointpropertydef_on_false	TEXT,
	waypointpropertydef_cat_id	INTEGER,
	--
	FOREIGN KEY (waypointpropertydef_cat_id) REFERENCES OzWalk.WaypointPropertyCategories(waypointpropertycat_id) ON DELETE CASCADE,
	PRIMARY KEY (waypointpropertydef_id)
);

CREATE TABLE OzWalk.Waypoints (	-- Nodes
	waypoint_id	SERIAL,
	waypoint_name	VARCHAR(64) NOT NULL,
	waypoint_latitude	DOUBLE PRECISION NOT NULL,
	waypoint_longitude	DOUBLE PRECISION NOT NULL,
	waypoint_desc	TEXT,
	waypoint_image	VARCHAR(73),	-- Length of "<MD5 hash>_<SHA-1 hash>".
	waypoint_url	VARCHAR(256),
	--
	PRIMARY KEY (waypoint_id)
);

CREATE TABLE OzWalk.WaypointProperties (
	waypointproperty_id	INTEGER,
	waypointproperty_propertydef_id	INTEGER,
	--
	FOREIGN KEY (waypointproperty_id) REFERENCES OzWalk.Waypoints(waypoint_id),
	FOREIGN KEY (waypointproperty_propertydef_id) REFERENCES OzWalk.WaypointPropertyDefinitions(waypointpropertydef_id) ON DELETE CASCADE,
	PRIMARY KEY (waypointproperty_id, waypointproperty_propertydef_id)
);	

CREATE TABLE OzWalk.WaypointEdges (	-- Edges
	-- waypointedge_id	SERIAL,
	waypointedge_waypoint_from	INTEGER NOT NULL,
	waypointedge_waypoint_to	INTEGER NOT NULL,
	waypointedge_distance_crow	INTEGER NOT NULL,
	waypointedge_distance_walk	INTEGER NOT NULL,
	waypointedge_gradient	SMALLINT DEFAULT 0,
	waypointedge_desc	TEXT NOT NULL,
	waypointedge_grade_exp	VARCHAR(2) NOT NULL,
	waypointedge_grade_fit	VARCHAR(2) NOT NULL,
	--
	CONSTRAINT edge_grade_exp CHECK (waypointedge_grade_exp IN ('NO', 'LI', 'QB', 'LO')),
	CONSTRAINT edge_grade_fit CHECK (waypointedge_grade_fit IN ('E', 'EM', 'M', 'MH', 'H')),
	CONSTRAINT edge_to_from_unique UNIQUE (waypointedge_waypoint_from, waypointedge_waypoint_to),
	FOREIGN KEY (waypointedge_waypoint_from) REFERENCES OzWalk.Waypoints(waypoint_id) ON DELETE CASCADE,
	FOREIGN KEY (waypointedge_waypoint_to) REFERENCES OzWalk.Waypoints(waypoint_id) ON DELETE CASCADE,
	-- PRIMARY KEY (waypointedge_id)
	PRIMARY KEY (waypointedge_waypoint_from, waypointedge_waypoint_to)
);

CREATE TABLE OzWalk.TrackGraph (	-- Paths
	trackgraph_id SERIAL,
	trackgraph_waypoint_from	INTEGER NOT NULL,
	trackgraph_waypoint_to	INTEGER NOT NULL,
	trackgraph_next_trackgraph_id	INTEGER,
	--
	FOREIGN KEY (trackgraph_waypoint_from, trackgraph_waypoint_to) REFERENCES OzWalk.WaypointEdges(waypointedge_waypoint_from, waypointedge_waypoint_to) ON DELETE CASCADE,
	FOREIGN KEY (trackgraph_next_trackgraph_id) REFERENCES OzWalk.TrackGraph(trackgraph_id) ON DELETE CASCADE,
	PRIMARY KEY (trackgraph_id)
);


CREATE TABLE OzWalk.Track (
	track_id	SERIAL,
	track_duration	SMALLINT DEFAULT 0,
	track_grade_exp	VARCHAR(2) NOT NULL,
	track_grade_fit	VARCHAR(2) NOT NULL,
	track_waypoint_start INTEGER NOT NULL,
	track_waypoint_finish	INTEGER NOT NULL,
	track_trackgraph_start_id	INTEGER NOT NULL,
	--
	CONSTRAINT track_grade_exp CHECK (track_grade_exp IN ('NO', 'LI', 'QB', 'LO')),
	CONSTRAINT track_grade_fit CHECK (track_grade_fit IN ('E', 'EM', 'M', 'MH', 'H')),
	FOREIGN KEY (track_waypoint_start) REFERENCES OzWalk.Waypoints(waypoint_id) ON DELETE CASCADE,
	FOREIGN KEY (track_waypoint_finish) REFERENCES OzWalk.Waypoints(waypoint_id) ON DELETE CASCADE,
	FOREIGN KEY (track_trackgraph_start_id) REFERENCES OzWalk.TrackGraph(trackgraph_id) ON DELETE CASCADE,
	PRIMARY KEY (track_id)
);

CREATE TABLE OzWalk.TrackLocations (	-- Reason why this is separated is because a track can be a part of many different regions.
	trackloc_track_id	INTEGER,
	trackloc_region_id	INTEGER,
	--
	FOREIGN KEY (trackloc_track_id) REFERENCES OzWalk.Track(track_id) ON DELETE CASCADE,
	FOREIGN KEY (trackloc_region_id) REFERENCES OzWalk.Regions(region_id) ON DELETE CASCADE,
	PRIMARY KEY (trackloc_track_id, trackloc_region_id)
);


CREATE TABLE OzWalk.Member (
	member_id	SERIAL,
	member_username	VARCHAR(32) UNIQUE NOT NULL,
	member_password	VARCHAR(60) NOT NULL,
	member_email	VARCHAR(256) UNIQUE NOT NULL,
	member_creation_date	TIMESTAMP DEFAULT LOCALTIMESTAMP,
	member_name_first	VARCHAR(64) NOT NULL,
	member_name_last	VARCHAR(64) NOT NULL,
	member_access_level	SMALLINT DEFAULT 1,
	member_walking_speed	REAL DEFAULT 3.2,
	member_timezone	REAL DEFAULT 0.0,
	--
	PRIMARY KEY (member_id)
);

CREATE TABLE OzWalk.LoginData (
	login_member_id	INTEGER,
	login_ip_addr	INET NOT NULL,
	login_timestamp	TIMESTAMP DEFAULT LOCALTIMESTAMP,
	login_used_cookie	BOOLEAN NOT NULL,
	--
	FOREIGN KEY (login_member_id) REFERENCES OzWalk.Member(member_id),
	PRIMARY KEY (login_member_id, login_ip_addr, login_timestamp)
);

CREATE TABLE OzWalk.RememberMeTokens (
	rmt_token_id	VARCHAR(64),
	rmt_member_id	INTEGER NOT NULL,
	--
	FOREIGN KEY (rmt_member_id) REFERENCES OzWalk.Member(member_id) ON DELETE CASCADE,
	PRIMARY KEY (rmt_token_id)
);

CREATE TABLE OzWalk.ForgotPasswordTokens (
	fpt_token_id	VARCHAR(64),
	fpt_member_id	INTEGER NOT NULL,
	fpt_set	TIMESTAMP DEFAULT LOCALTIMESTAMP,
	--
	FOREIGN KEY (fpt_member_id) REFERENCES OzWalk.Member(member_id) ON DELETE CASCADE,
	PRIMARY KEY (fpt_token_id)
);

CREATE TABLE OzWalk.SiteLogs (
	log_id	BIGSERIAL,
	log_timestamp	TIMESTAMP DEFAULT LOCALTIMESTAMP,
	log_ip_addr	INET NOT NULL,
	log_member_id	INTEGER,
	log_path	VARCHAR(256) NOT NULL,
	--
	FOREIGN KEY (log_member_id) REFERENCES OzWalk.Member(member_id),
	PRIMARY KEY (log_id)
);

-- INDEXES

CREATE INDEX Index_Countries ON OzWalk.Countries USING hash (country_id);
CREATE INDEX Index_States ON OzWalk.States USING hash (state_id);
CREATE INDEX Index_Regions ON OzWalk.Regions USING hash (region_id);
CREATE INDEX Index_WaypointTypes_ID ON OzWalk.WaypointTypes USING hash (waypointtype_id);
CREATE INDEX Index_WaypointTypes_Name ON OzWalk.WaypointTypes USING hash (waypointtype_name);
CREATE INDEX Index_WaypointPropertyCategories ON OzWalk.WaypointPropertyCategories USING hash (waypointpropertycat_id);
CREATE INDEX Index_WaypointPropertyDefinitions ON OzWalk.WaypointPropertyDefinitions USING hash (waypointpropertydef_id);
CREATE INDEX Index_Waypoints_ID ON OzWalk.Waypoints USING hash (waypoint_id);
CREATE INDEX Index_Waypoints_Name ON OzWalk.Waypoints USING hash (waypoint_name);
CREATE INDEX Index_Track_ID ON OzWalk.Track USING hash (track_id);
CREATE INDEX Index_TrackGraph_ID ON OzWalk.TrackGraph USING hash (trackgraph_id);
CREATE INDEX Index_Member_ID ON OzWalk.Member USING hash (member_id);
CREATE INDEX Index_Member_Username ON OzWalk.Member USING hash (member_username);
CREATE INDEX Index_Member_Email ON OzWalk.Member USING hash (member_email);
CREATE INDEX Index_LoginData_ID ON OzWalk.LoginData USING hash (login_member_id);
CREATE INDEX Index_RememberMeTokens ON OzWalk.RememberMeTokens USING hash (rmt_token_id);
CREATE INDEX Index_ForgotPasswordTokens ON OzWalk.ForgotPasswordTokens USING hash (fpt_token_id);
CREATE INDEX Index_SiteLogs_ID ON OzWalk.SiteLogs USING hash (log_id);
CREATE INDEX Index_SiteLogs_MemberID ON OzWalk.SiteLogs USING hash (log_member_id);

CREATE UNIQUE INDEX Index_WaypointProperties ON OzWalk.WaypointProperties USING btree (waypointproperty_id, waypointproperty_propertydef_id);
CREATE UNIQUE INDEX Index_WaypointEdges_FromTo ON OzWalk.WaypointEdges USING btree (waypointedge_waypoint_from, waypointedge_waypoint_to);
CREATE UNIQUE INDEX Index_WaypointEdges_Grades ON OzWalk.WaypointEdges USING btree (waypointedge_grade_exp, waypointedge_grade_fit);
CREATE UNIQUE INDEX Index_Track_Grades ON OzWalk.Track USING btree (track_grade_exp, track_grade_fit);
CREATE UNIQUE INDEX Index_Track_Duration ON OzWalk.Track USING btree (track_duration);
CREATE UNIQUE INDEX Index_LoginData_Timestamp ON OzWalk.LoginData USING btree (login_timestamp);
CREATE UNIQUE INDEX Index_SiteLogs_Timestamp ON OzWalk.SiteLogs USING btree (log_timestamp);
CREATE UNIQUE INDEX Index_SiteLogs_IP ON OzWalk.SiteLogs USING btree (log_ip_addr);

-- STORED PROCEDURES 

CREATE OR REPLACE FUNCTION OzWalk.GetCountries() RETURNS TABLE (country_id INTEGER, country_name VARCHAR(64)) AS $$
BEGIN
	RETURN QUERY
		SELECT c.country_id, c.country_name
		FROM OzWalk.Countries c
		ORDER BY c.country_name ASC;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION OzWalk.GetAllStates() RETURNS TABLE (country_id INTEGER, country_name VARCHAR(64), state_id INTEGER, state_name VARCHAR(64)) AS $$
BEGIN
	RETURN QUERY
		SELECt c.country_id, c.country_name, s.state_id, s.state_name
		FROM OzWalk.States s
		JOIN OzWalk.Countries c ON (s.country_id = c.country_id)
		ORDER BY c.country_name, s.state_name ASC;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION OzWalk.GetAllRegions() RETURNS TABLE (country_id INTEGER, country_name VARCHAR(64), state_id INTEGER, state_name VARCHAR(64), region_id INTEGER, region_name VARCHAR(128)) AS $$
BEGIN
	RETURN QUERY
		SELECT c.country_id, c.country_name, s.state_id, s.state_name, r.region_id, r.region_name
		FROM OzWalk.Regions r
		JOIN OzWalk.States s ON (r.state_id = s.state_id)
		JOIN OzWalk.Countries c ON (s.country_id = c.country_id)
		ORDER BY c.country_name, s.state_name, r.region_name ASC;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION OzWalk.InsertCountry(country_name VARCHAR(64)) RETURNS VOID AS $$
BEGIN
	INSERT INTO OzWalk.Countries(country_name) VALUES (country_name);
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION OzWalk.InsertState(country_id INTEGER, state_name VARCHAR(64), state_abbr VARCHAR(8)) RETURNS VOID AS $$
BEGIN
	IF(country_id IS NULL OR state_name IS NULL OR state_abbr IS NULL) THEN
		IF(country_id IS NULL) THEN
			RAISE EXCEPTION 'The new `state` must belong to a country.';
		ELSIF(state_name IS NULL) THEN
			RAISE EXCEPTION 'You must enter a state name. It cannot be empty.';
		ELSIF(state_abbr IS NULL) THEN
			RAISE EXCEPTION 'You must enter a state abbreviation. It cannot be empty.';
		END IF;
	ELSE
		BEGIN
			INSERT INTO OzWalk.States(state_name, state_abbr, country_id) VALUES (state_name, state_abbr, country_id);
			EXCEPTION
				WHEN foreign_key_violation THEN
					RAISE EXCEPTION 'The country with ID % does not exist.', country_id;
				WHEN unique_violation THEN
					RAISE EXCEPTION 'A state with the same name already belongs to that country.';
				WHEN OTHERS THEN
					RAISE EXCEPTION 'An error has occured.';
		END;
	END IF;
	
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION OzWalk.InsertRegion(state_id INTEGER, region_name VARCHAR(128)) RETURNS VOID AS $$
BEGIN
	IF(state_id IS NULL OR region_name IS NULL) THEN
		IF(state_id IS NULL) THEN
			RAISE EXCEPTION 'The new `region` must belong to a state.';
		ELSIF(region_name IS NULL) THEN
			RAISE EXCEPTION 'You must enter a region name. It cannot be empty.';
		END IF;
	ELSE
		BEGIN
			INSERT INTO OzWalk.Regions(region_name, state_id) VALUES (region_name, state_id);
			EXCEPTION
				WHEN foreign_key_violation THEN
					RAISE EXCEPTION 'The state with ID % does not exist.', state_id;
				WHEN unique_violation THEN
					RAISE EXCEPTION 'A region with the same name already belongs to that state.';
				WHEN OTHERS THEN
					RAISE EXCEPTION 'An error has occurred.';
		END;
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION OzWalk.InsertWaypointType(waypointtype_name VARCHAR(64), waypointtype_style VARCHAR(128), waypointtype_parent INTEGER) RETURNS VOID AS $$
BEGIN
	IF(waypointtype_name IS NULL) THEN
		RAISE EXCEPTION 'Your new waypoint category must have a name. It cannot be empty.';
	ELSE
		BEGIN
			INSERT INTO OzWalk.WaypointTypes(waypointtype_name, waypointtype_style, waypointtype_parent_id) VALUES (waypointtype_name, waypointtype_style, waypointtype_parent);
			EXCEPTION
				WHEN foreign_key_violation THEN
					RAISE EXCEPTION 'The waypoint category parent with ID % does not exist.', waypointtype_parent;
				WHEN unique_violation THEN
					RAISE EXCEPTION 'A waypoint category with the same name already exists.';
				WHEN OTHERS THEN
					RAISE EXCEPTION 'An error has occurred.';
		END;
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION OzWalk.InsertWaypointPropertyCategory(waypointpropertycat_name VARCHAR(64), waypointpropertycat_text_prepend TEXT, waypointpropertycat_append TEXT, waypointpropertycat_waypointtype_id INTEGER) RETURNS VOID AS $$
BEGIN
	IF(waypointpropertycat_name IS NULL OR waypointpropertycat_waypointtype_id IS NULL) THEN
		IF(waypointpropertycat_name IS NULL) THEN
			RAISE EXCEPTION 'You must enter a waypoint property category name. It cannot be empty.';
		ELSIF(waypointproprtycat_waypointtype_id IS NULL) THEN
			RAISE EXCEPTION 'The waypoint property category must belong to a waypoint category.';
		END IF;
	ELSE
		BEGIN
			INSERT INTO OzWalk.WaypointPropertyCategories(waypointpropertycat_name, waypointpropertycat_text_prepend, waypointpropertycat_text_append, waypointpropertycat_waypointtype_id) VALUES (
				waypointpropertycat_name, waypointpropertycat_text_prepend, waypointpropertycat_text_append, waypointpropertycat_waypointtype_id);
			EXCEPTION
				WHEN foreign_key_violation THEN
					RAISE EXCEPTION 'The waypoint category ID % does not exist.', waypointpropertycat_waypointtype_id;
				WHEN OTHERS THEN
					RAISE EXCEPTION 'An error has occurred.';
		END;
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION OzWalk.InsertWaypointPropertyDefinitions(waypointpropertydef_name VARCHAR(64), waypointpropertydef_on_true TEXT, waypointpropertydef_on_false TEXT, waypointpropertydef_cat_id INTEGER) RETURNS VOID AS $$
BEGIN
	IF(waypointpropertydef_name IS NULL OR waypointpropertydef_cat_id IS NULL) THEN
		IF(waypointpropertydef_name IS NULL) THEN
			RAISE EXCEPTION 'You must enter a waypoint property name. It cannot be empty.';
		ELSIF(waypointpropertydef_cat_id IS NULL) THEN
			RAISE EXCEPTION 'The waypoint property must belong to a waypoint property category.';
		END IF;
	ELSIF(waypointpropertydef_on_true IS NULL AND waypointpropertydef_on_false IS NULL) THEN
		RAISE EXCEPTION 'You must enter something in on_true or on_false. Both cannot be blank.';
	ELSE
		BEGIN
			INSERT INTO OzWalk.WaypointPropertyDefinitions(waypointpropertydef_name, waypointpropertydef_on_true, waypointpropertydef_on_false, waypointpropertydef_cat_id) VALUES (
				waypointpropertydef_name, waypointpropertydef_on_true, waypointpropertydef_on_false, waypointpropertydef_cat_id);
			EXCEPTION
				WHEN foreign_key_violation THEN
					RAISE EXCEPTION 'The waypoint property category with ID % does not exist.', waypointpropertydef_cat_id;
				WHEN unique_violation THEN
					RAISE EXCEPTION 'A waypoint property with the same name already exists.';
				WHEN OTHERS THEN
					RAISE EXCEPTION 'An error has occured.';
		END;
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE REPLACE FUNCTION OzWalk.InsertWaypoint(waypoint_name VARCHAR(64), waypoint_latitude DOUBLE PRECISION, waypoint_longitude DOUBLE PRECISION, waypoint_desc TEXT, waypoint_image VARCHAR(73), waypoint_url VARCHAR(256)) AS $$
BEGIN
	-- ToDo
END;
$$ LANGUAGE plpgsql;
--

COMMIT;
